# ansible-role-kiosk

This role will install and configure a kiosk browser for digital signage and
interactive applications.

## Role Variables

By default the role installs the latest version of Chromium available in the
repositories. On 22.04+ this is a dummy package for snap. This role will
migrate to using snap in the future.

In the meantime you can force a specific version by setting this variable:

```yaml
kiosk_browser_version: "=65.0.3325.181-0ubuntu1"
```

See for more info:

- [Ansible apt module docs](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#parameter-name)
- [apt-get manual](https://manpages.ubuntu.com/manpages/xenial/man8/apt-get.8.html#description)

Configure one or more kiosk screens based on either URL or content from a
Nextcloud server on schedule:

```yaml
kiosk_instances:
  - id: 1
    contentdir: testnuc-cmp-1
    content: media-1234.zip
    zipdir: app-content
    index: index.html
    source: nextcloud
    schedule:
      - 'Mon..Fri *-*-* *:*:00'
  - id: 2
    url: https://dashboard.naturalis.nl
    schedule:
      - 'Sat..Sun *-*-* *:*:00'
    config:
      height: 600
      width: 800
```

Based on schedule entries systemd timer units will be configured. Make sure
that these don't overlap.

## Dependencies

This role depends on the
[ansible-role-interactive](https://gitlab.com/naturalis/mii/ansible-role-interactive)
role.

## Example playbook

You can use this role like this:

```yaml
- hosts: all
  gather_facts: true
  become: true
  tasks:
    - name: Apply kiosk role
      ansible.builtin.include_role:
        name: kiosk
        apply:
          tags: kiosk
      tags: always
```

## License

Apache

## Author Information

David Heijkamp working for the museum at Naturalis Biodiversity Center.
